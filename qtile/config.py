from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Screen, Match
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.widget import volume
from os.path import expanduser
import subprocess, re

from pwvol import PWVolume as Pipevol

mod = "mod4"
terminal = guess_terminal()
browser = "firefox"

# Change colors. Leave this empty (colors = []) to
# use pywal colors instead
# colors = [
#    ["#2e3440", "#2e3440"],  # background
#    ["#bf616a", "#bf616a"],  # red
#    ["#a3be8c", "#a3be8c"],  # green
#    ["#ebcb8b", "#ebcb8b"],  # yellow
#    ["#81a1c1", "#81a1c1"],  # blue
#    ["#b48ead", "#b48ead"],  # magenta
#    ["#88c0d0", "#88c0d0"],  # cyan
#    ["#d8dee9", "#d8dee9"],  # foreground
#    ["#3b4252", "#3b4252"],  # background lighter
#    ["#4c566a", "#4c566a"],  # grey
# ]
colors = [
    ["#292d3e", "#292d3e"],  # background
    ["#f07178", "#f07178"],  # red
    ["#c3e88d", "#c3e88d"],  # green
    ["#ffcb6b", "#ffcb6b"],  # yellow
    ["#82aaff", "#82aaff"],  # blue
    ["#c792ea", "#c792ea"],  # magenta
    ["#89ddff", "#89ddff"],  # cyan
    ["#a6accd", "#a6accd"],  # foreground
    ["#34324a", "#34324a"],  # background lighter
    ["#676e95", "#676e95"],  # grey
]

if colors == []:
    cache = expanduser("~/.cache/wal/colors")

    def load_colors(cache):
        with open(cache, "r") as file:
            for i in range(9):
                colors.append(file.readline().strip())
        lazy.reload()

    load_colors(cache)
    colors.append("#808080")  # TODO Lighter bg color calculation

keys = [
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod, "shift"], "h", lazy.layout.swap_left()),
    Key([mod, "shift"], "l", lazy.layout.swap_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "o", lazy.layout.shrink()),
    Key([mod], "u", lazy.layout.reset()),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "Return", lazy.spawn(terminal)),
    Key([mod], "b", lazy.spawn(browser)),
    Key([mod], "t", lazy.spawn("telegram-desktop")),
    Key([mod], "m", lazy.spawn("emacs")),
    Key([mod], "s", lazy.spawn("steam")),
    Key([], "Print", lazy.spawn("flameshot gui")),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "space", lazy.window.toggle_floating(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "d", lazy.spawncmd()),
]

groups = [Group(i) for i in "12345"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(toggle=False),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.MonadTall(
        border_focus="#4c566a", border_normal="#3b4252", border_width=3, margin=8
    ),
]

widget_defaults = dict(
    font="Fira Code Nerd Font Mono", fontsize=14, padding=3, background=colors[8]
)
extension_defaults = widget_defaults.copy()


def getkbl():
    return (
        subprocess.check_output(["xkblayout-state", "print", '"%s"'])
        .decode("utf-8")
        .strip()[1:3]
    )


class PipewireVol(volume.Volume):
    def get_volume(self):
        try:
            get_volume_cmd = self.create_amixer_command("sget", self.channel)

            if self.get_volume_command:
                get_volume_cmd = self.get_volume_command

            mixer_out = self.call_process(get_volume_cmd)
        except subprocess.CalledProcessError:
            return -1

        return int(mixer_out)


screens = [
    Screen(
        wallpaper="~/Pictures/Wallpapers/palenight.jpg",
        wallpaper_mode="fill",
        top=bar.Bar(
            [
                widget.Image(filename="~/Pictures/tomato.png", margin=4),
                widget.GroupBox(
                    active=colors[7],
                    inactive=colors[9],
                    highlight_color=colors[8],
                    block_highlight_text_color=colors[4],
                    highlight_method="line",
                    this_current_screen_border=colors[0],
                    foreground=colors[7],
                    urgent_border=colors[1],
                    disable_drag=True,
                ),
                widget.TextBox(text="", foreground=colors[0], padding=0, fontsize=24),
                widget.Prompt(background=colors[0]),
                widget.WindowName(background=colors[0]),
                widget.Systray(background=colors[0]),
                widget.TextBox(text="", foreground=colors[0], padding=0, fontsize=24),
                widget.GenPollText(func=getkbl, update_interval=0.1),
                Pipevol(
                    get_volume_command=["pamixer", "--get-volume"],
                    fmt="V:{}",
                ),
                widget.Battery(format="B:{percent:2.0%}"),
                widget.Clock(format="%a %H:%M"),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="file_progress"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
