# vim:fdm=marker

{ config, pkgs, lib, ... }:

{
  # Packages {{{

  home.packages = let
    atlauncher = rec {
      version = "3.4.8.1";
      bin = builtins.fetchurl "https://github.com/ATLauncher/ATLauncher/releases/download/v${version}/ATLauncher-${version}.jar";
      package = pkgs.writeShellScriptBin "atlauncher" ''
        java -jar ${bin} --working-dir ${config.xdg.dataHome}/atlauncher
      '';
    };
  in with pkgs; [
    asciinema
    # Looks / Usability
    pfetch
    flameshot
    acpi
    xkblayout-state
    libnotify
    pywal
    libsForQt5.qtstyleplugins
    nordic
    pavucontrol
    unzip
    # Replacements for standart tools
    exa
    bat
    bpytop
    archiver
    fd
    prettyping
    # Media
    sxiv
    zathura
    obs-studio
    qbittorrent
    nicotine-plus
    imagemagick
    ffmpeg
    yt-dlp
    # Games
    atlauncher.package
    minetest
    (multimc.override { msaClientID = "e56cb3be-4586-4575-94be-9fa3b8cef665"; })
    tintin
    cataclysm-dda
    the-powder-toy
    protonup
    # Entertainment
    freetube
    # Social
    discord
    tdesktop
    # Work / Produtivity
    firefox
    emacs
    tectonic
    texlive.combined.scheme-full
    krita
    olive-editor
    bitwarden
    blender
    # Coding
    fasm nasm gnumake     # Assembly
    clang cmake ninja     # C
    python3 black         # Python
    eclipses.eclipse-java # Java
    dbeaver               # SQL
    flutter dart          # Flutter
    nixpkgs-fmt rnix-lsp  # Nix
    nodejs                # I don't use node, but dap-mode needs this
    # Cartography
    josm
    qgis
    # Money
    bisq-desktop
    # Wine
    wineWowPackages.staging
    (winetricks.override { wine = wineWowPackages.staging; })
    geckodriver
    mono
    # Other
    android-tools mtpfs
    nix-prefetch-scripts
    nix-prefetch-github
    # Fonts
    comic-relief
  ];
# }}}

  # Zsh {{{
  programs.zsh = {
    enable = true;
    enableCompletion = false;
    dotDir = ".config/zsh";
    shellAliases = {
      sudo = "doas ";
      doas = "doas "; # Makes aliases work under root
      apply = "exec zsh";
      clear = "clear && echo && pfetch";
      i = "nvim ~/.config/nixpkgs/home.nix";
      ia = "home-manager switch";
      s = "sv /etc/nixos/configuration.nix";
      sa = "sudo nixos-rebuild switch";
      v = "$EDITOR";
      sv = "function _(){ext=\"$(echo $1|rev|cut -d. -f1|rev)\" && cp \"$1\" \"/tmp/doase.$ext\" && v \"/tmp/doase.$ext\" && doas cp \"/tmp/doase.$ext\" \"$1\" && rm \"/tmp/doase.$ext\"}; _"; # Like sudoedit, but for doas
      grep = "rg";
      ls = "exa --icons";
      cat = "bat";
      c = "clear";
      update = "sudo nixos-rebuild switch --upgrade && home-manager switch";
      rb = "reboot";
      x = "exit";
      killall = "pkill -KILL";
      ping = "prettyping";
      find = "fd";

      nex = "source ~/Documents/Code/lcthw/nex";
    };
    zplug = {
      enable = true;
      plugins = [
        { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; }
        { name = "zsh-users/zsh-autosuggestions"; }
        { name = "marlonrichert/zsh-autocomplete"; tags = [ defer:2 ]; }
        { name = "zdharma/fast-syntax-highlighting"; }
        { name = "ael-code/zsh-colored-man-pages"; }
      ];
    };
    initExtraFirst = ''
      clear
      echo
      export PF_INFO="ascii title os uptime shell memory"
      pfetch
      if [[ -r "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh" ]]; then
        source "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh"
      fi
      '';
    initExtra = ''
      zstyle ':completion:*:paths' path-completion yes
      [[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
      bindkey "''${key[Up]}" up-line-or-search
      '';
    history = {
      size = 10000000;
      save = 10000000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
  };

  home.sessionPath = [ "$HOME/.emacs.d/bin" "$HOME/.local/bin" "$HOME/.local/bin/dalias" ];
  # }}}

  # Declutter $HOME {{{
  home.sessionVariables = rec {
    XCOMPOSECACHE    = "${config.xdg.cacheHome}/xcompose";
    STACK_ROOT       = "${config.xdg.dataHome}/stack";
    GOPATH           = "${config.xdg.dataHome}/go";
    MYSQL_HISTFILE   = "${config.xdg.dataHome}/mysql_history";
    CUDA_CACHE_PATH  = "${config.xdg.cacheHome}/nv";
    SSB_HOME         = "${config.xdg.dataHome}/zoom";
    GRADLE_USER_HOME = "${config.xdg.dataHome}/gradle";
    _JAVA_OPTIONS    = "-Djava.util.prefs.userRoot=${config.xdg.configHome}/java";
    ANDROID_HOME     = "${config.xdg.dataHome}/Android/Sdk";
    PUB_CACHE        = "${config.xdg.cacheHome}/pub";
  };
  gtk.gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
  xresources.path = "${config.xdg.configHome}/X11/Xresources";
  # }}}

  # Neovim {{{
  programs.neovim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      nord-vim
      palenight-vim
      vim-nix
    ];
    extraConfig = ''
      set clipboard+=unnamedplus
      set splitbelow
      set splitright
      set laststatus=1
      set number
      set iskeyword-=_
      set iskeyword+=/
      set noshowmode
      set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
      set mouse=a
      set background=dark
      colorscheme palenight
      set termguicolors
      hi visual cterm=reverse
      noremap <silent> <ESC> :noh<CR>
      
      autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ |   exe "normal! g`\""
        \ | endif
      '';
  };
  home.sessionVariables.EDITOR = "nvim";
  # }}}

  # Pidgin {{{
  programs.pidgin = {
    enable = true;
    plugins = with pkgs; [
      purple-discord
    ];
  };
  # }}}

  # mpv {{{
  programs.mpv = {
    enable = true;
    bindings = {
      "Alt+j" = "add sub-scale -0.1";
      "Alt+k" = "add sub-scale +0.1";
    };
  };
  # }}}

  # Dunst {{{
  services.dunst = {
    enable = true;
    settings = {
      global = {
        font = "FiraCode Nerd Font 10.5";
        horizontal_padding = 8;
        format = "%a | %s: %b";
        max_icon_size = 20;
      };
      urgency_normal = {
        background = "#292d3e";
        foreground = "#a6accd";
        timeout = 5;
      };
      urgency_critical = {
        background = "#f07178";
        foreground = "#a6accd";
      };
      urgency_low = {
        background = "#82aaff";
        foreground = "#a6accd";
      };
    };
  };
  ### }}}

  # lf {{{
  programs.lf = {
    enable = true;
    keybindings = {
      D = ''$rm -rf "$fx"'';
    };
    settings = {
      hidden = true;
    };
  };
  # }}}

  # Lorri and Direnv {{{
  services.lorri.enable = true;
  programs.direnv.enable = true;
  home.sessionVariables.DIRENV_LOG_FORMAT = "";
  # }}}

  # Git {{{
  programs.git = {
    enable = true;
    userName = "flexagoon";
    userEmail = "flexagoon@pm.me";
    extraConfig = {
      init.defaultBranch = "main";
      credential.helper = "store --file=${config.xdg.configHome}/git/credentials";
      push.default = "current";
    };
  };
  # }}}

  # Picom {{{
  services.picom = { 
    enable = true;
    experimentalBackends = true;
    backend = "xrender";
    vSync = true;
    extraOptions = "unredir-if-possible = true;";
  };
  # }}}

  # Xresources {{{
  xresources = {
    extraConfig = builtins.readFile ''${builtins.fetchGit "https://github.com/arcticicestudio/nord-xresources"}/src/nord'';
    properties = {
      "*.alpha" = 1;
      "*.font" = "FiraCode Nerd Font Mono:pixelsize=19:antialias=true:autohint=true";
    };
  };
  # }}}

  xdg.enable = true;

  systemd.user.startServices = true;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "discord"
  ];

  programs.home-manager.enable = true;
  home.username = "flexagoon";
  home.homeDirectory = "/home/flexagoon";
  home.stateVersion = "21.11";
}
